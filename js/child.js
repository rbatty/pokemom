pokemom.Child = function(name){
		this.name = name || "Harold";
		this.condition = either(["sickly","healthy","squirmy"]);
		this.gender = either(["boy","girl"]);
		//this.pronoun:"",
		//this.possessive_pronoun: "",
		this.canEncounter = false;
		this.difficulty = either(["exhausting","unremarkable","easy"]);
		this.temperment = either(["fussy","calm","curious"]);
		this.image = "img/baby_calm.gif";

		this.max_stamina = setMaxStamina(this.condition);
		this.stamina = this.max_stamina;

		this.modifier = setModifier(this.temperment);

		this.resilience = 0;
		//this.resilienceUp = 5;
		this.resilienceArray = [];
		this.intelligence = 0;
		//this.intelligenceUp = 5;
		this.intelligenceArray = [{skill: "Tell story", shortname: "story", type: "intelligence", level: 1, gainedAt: 5, active: false, id: 0}];
		this.empathy = 0;
		//this.empathyUp = "?";
		this.empathyArray = [{skill: "Sing song", shortname: "song", type: "empathy", level: 1, gainedAt: 5, active: false, id: 0}];
		this.bond = 0;
		//this.bondUp = "?";
		this.bondArray = [{skill: "Get hug", shortname: "healing hug", type: "bond", level: 1, gainedAt: 4, active: false, id: 0}];
		this.skillArray = [];

		function either(options){
			var rand = Math.floor(Math.random()*options.length);
			var choosen = options[rand];
			return choosen;
		}

		function setMaxStamina(condition){
			return (condition === "sickly" ? 8: 10);
		}

		function setModifier(temperment){
			var _modifier = 1;
			if (temperment === "fussy"){_modifier = 0.8 };
			if (temperment === "curious"){_modifier = 1.2};
			return _modifier;
		}

		function trainingSuccess(s){
			var _roll = 0;
			if (s.temperment === "fussy") {
				//1 in 3 chance of failing
				_roll = Math.ceil(Math.random()*3);
			} else if (s.temperment === "calm") {
				//1 in 5 chance
				_roll = Math.ceil(Math.random()*5);
			}
			return (_roll === 1 ? false: true);
		}

		function levelUp(){

		}

		function skillGain(s, stat){
			//returns empty if no skill gained.
			//var statUp = stat+"Up";
			var statArray = s[stat+"Array"];
			var skillType = "";
			var message = "";
			for (var i=0; i < statArray.length; i++){
				//console.log(statArray[i]);
				if (s[stat] >= statArray[i].gainedAt && !statArray[i].active ){
					statArray[i].active = true;
					s.skillArray.push(statArray[i]);
					skillType = statArray[i].shortname;
					if (stat !== "bond") { s.canEncounter = true; }
					message += "Your "+s.gender+" has learned a "+skillType.toUpperCase()+"! ";
					message += "You feel so proud of your little " + s.gender+".";
				}
			}
			
			return message;
		}

		this.raiseStat = function(stat){
			var _message = "Your "+this.gender+" fusses and squirms.";
			var _image = "img/baby_cry.gif";
			if (trainingSuccess(this)) {
				this[stat] += this.modifier;
				_message = "Your "+this.gender+" is getting more powerful!";
				var _image = "img/baby_calm.gif";
			} else {
				console.log("failed");
			}

			//check if skill gained
			var skillResult = skillGain(this, stat);
			if (skillResult) {
				_message += "<br />"+skillResult;
				//_image = level_up_img;
			}
			return {message: _message, image: _image};
		}

		this.debugStats = function(){
			//return {int: this.intelligence, emp: this.empathy};
			console.log(this.temperment+": "+this.intelligence + ","+ this.empathy +","+this.bond+","+this.resilience);
		}

		return this;
	};