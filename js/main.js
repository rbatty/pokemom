var pokemom = (function(){
	//some globals
	var display, results, success, child, player, opponent;

	//some classes
	var Player = function(name){
		this.name = name || "";
		this.parent = "";
		this.children = [];
	};

	var Opponent = function(type){
		var all = [
			{name: "Dewey", epithet: "nephew Dewey", weakness: "intelligence", stamina: "3,6", attacks:[{name: "cries", power: 2}], image_calm:"img/baby2.gif"},
			{name: "Katrina", epithet: "baby Katrina", weakness: "empathy", stamina: "4,6", attacks:[{name: "cries", power: 3}], image_calm:"img/babyg_calm.gif"}
		]
		var init = either(all);
		this.name = init.name;
		this.epithet = init.epithet;
		this.weakness = init.weakness;
		this.max_stamina = either(init.stamina.split(","));
		this.stamina = this.max_stamina;
		this.image = init.image_calm;
		this.attacks = init.attacks;

		function either(options){
			var rand = Math.floor(Math.random()*options.length);
			var choosen = options[rand];
			return choosen;
		}
	}

	var state = {
		state: "waiting",
		action: "init"
	};
	function init() {
		//Initialize variables
		//display = document.getElementById("game");
		display = {
			game: document.getElementById("game"),
			messages: document.getElementById("messages"),
			images: document.getElementById("images"),
			options: document.getElementById("options")
		}

		//Display start screen
		var startup = "<h1>POKÉMOM</h1>";
		display.messages.innerHTML = startup;
		buttonCreate("Start", "start");
		//buttonCreate("Continue", "cont");
	}

	function clearScreen() {
		display.messages.innerHTML = "";
		display.images.innerHTML = "";
		display.options.innerHTML = "";
	}

	function newGame() {
		clearScreen();
		player = new Player();
		child = new pokemom.Child();
		var _message = "<p>Congratulations! "
		if (player.parent === "father"){
			_message += "Today your first child, a "+child.condition+" "+child.gender+", was born. ";
		} else {
			_message += "Today you gave birth to your first child, a "+child.condition+" "+child.gender+". ";
		}
		_message += "It was an "+child.difficulty+" birth and the child is "+child.temperment+".</p>";
		display.messages.innerHTML = _message;
		var _image = document.createElement("img");
		_image.src = child.image;
		display.images.appendChild(_image);
		babyTrainingButtons();
	}

	function buttonCreate(text, action, stat){
		var _buttonContainer = document.createElement("div");
		_buttonContainer.setAttribute("class", "button");
		var _button = document.createElement("p");
		_button.innerHTML = text;
		_button.action = action;
		if (stat) {_button.stat = stat;}
		_buttonContainer.appendChild(_button);
		display.options.appendChild(_buttonContainer);
		_buttonContainer.appendChild(_button);
		_button.addEventListener("click", buttonAction, false);
	}

	function actionCreate(text, type, level){
		var _buttonContainer = document.createElement("div");
		_buttonContainer.setAttribute("class", "button");
		var _button = document.createElement("p");
		_button.innerHTML = text;
		_button.type = type;
		_button.level = level;
		_buttonContainer.appendChild(_button);
		display.options.appendChild(_buttonContainer);
		_buttonContainer.appendChild(_button);
		_button.addEventListener("click", babyEncounter, false);
	}

	function buttonAction(e){
		var action = e.target.action;
		var stat = e.target.stat;
		switch (action) {
			case "start":
				newGame();
			break;
			case "cont":
				loadGame();
			break;
			case "read":
			case "sing":
			case "cuddle":
			case "play":
				var result = child.raiseStat(stat);
				babyTraining(result);
			break;
			case "playdate":
				babyEncounter();
			break;
			case "enddate":
				babyTraining();
			break;
		}
	}

	function babyTraining(result){
		clearScreen();
		var img = document.createElement("img");
		if (result) {
			display.messages.innerHTML = "<p>" + result.message + "</p>";
			img.src=result.image;
		} else {
			var _message = "So now you've got this teeny pink little bundle, looking at you helplessly ";
			_message += "through half-opened eyes. No better time than the present to start preparing ";
			_message += "it to face the world. What do you do?";
			display.messages.innerHTML = "<p>" + _message + "</p>";
			img.src=child.image;
		}
		display.images.appendChild(img);
		babyTrainingButtons();
	}

	function babyTrainingButtons(){
		buttonCreate("Read", "read", "intelligence");
		buttonCreate("Sing", "sing", "empathy");
		buttonCreate("Cuddle", "cuddle", "bond");
		buttonCreate("Play", "play", "resilience");
		if(child.canEncounter){
			buttonCreate("Schedule a playdate", "playdate");
		}
	}

	function babyActions(){
		//draws the actions available to the child
		for (var i=0; i < child.skillArray.length; i++) {
			actionCreate(child.skillArray[i].skill, child.skillArray[i].type, child.skillArray[i].level);
		}
	}

	function babyEncounter(e){
		clearScreen();
		var _message = "";
		//TODO: remove event listeners
		if (e) {
			var type = e.target.type;
			var level = e.target.level
			//var message_div = document.getElementById("message");

			if (type === "bond") {
				//healing effects
				child.stamina += child[type] * level;
				_message = "You child feels renewed!";
			} else {
				if (opponent.weakness === type) {
					opponent.stamina -= (child[type]*0.25) * level * 1.5;
					_message = opponent.name + " was thoroughly amused!";
				} else {
					opponent.stamina -= (child[type]*0.25) * level;
					_message = opponent.name + " seems to enjoy that.";
				}
			}

			console.log("child: "+child.stamina+ ", enemy: "+opponent.stamina);

			display.messages.innerHTML = "<p>" + _message + "</p>";

			var textPause = setTimeout(function(){
				if (opponent.stamina <= 0){
					_message = "Your "+child.gender+" had a great playdate!";
					child.level++;
					//display.innerHTML = "<p>" + _message + "</p>";
					var opponent_img = document.getElementById("opponent");
					display.images.removeChild(opponent_img);
					display.messages.innerHTML = "<p>" + _message + "</p>";
					opponent = {};
					buttonCreate("End playdate", "enddate");
					return;
				} else {
					//enemy takes turn
					_message = opponent.name+" "+opponent.attacks[0].name + ". Your "+child.gender+" is upset. ";
					var randModifer = Math.random()+.6;
					child.stamina-=opponent.attacks[0].power * randModifer;
					console.log("child: "+child.stamina+ ", enemy: "+opponent.stamina);

				//"$enemy_name hoardes the toys. Your $gender is sad.
				}
				if (child.stamina <= 0) {
					_message = "Your child is exhausted by " + opponent.epithet+".";
					buttonCreate("End playdate", "enddate");
				}

				display.messages.innerHTML = "<p>" + _message + "</p>";

			}, 1500);

		} else {
			//initialize opponent
			opponent = new Opponent("baby");

			_message = "Your "+child.gender+" has a play date with "+opponent.epithet+"!<br />";
			_message += "What do you think your "+child.gender+" should do?";
			
		}

		//var message_div = document.createElement("div");
		//message_div.id = "message";
		display.messages.innerHTML = "<p>" + _message + "</p>";
		//display.innerHTML = "";
		//display.appendChild(message_div);
	//draw child
		var img = document.createElement("img");
		img.src=child.image;
		display.images.appendChild(img);
		//draw opponent
		var img2 = document.createElement("img");
		img2.id = "opponent";
		img2.src=opponent.image;
		display.images.appendChild(img2);

		//draw buttons
		babyActions();
	}

	function loadGame(){
		//TODO: load save data
	}

	return {
		init: init
	}
})()