# PokéMom
(version 0.0.2)
##A momjam game

Being a parent is tough. Do you wanna be the very best? Like no one ever was? Then see if you have what it takes to be a PokéMom!

This game was created for a Mother's Day game jam, but let's face it, dad's are awesome, too. So even though it's called PokéMom, you can play as either a mother or father with no real effect on gameplay.

###Tech Specs
Language: Javascript

Compatible with most modern browsers.

###Release Notes
####Version 0.0.1
######Current Features
- Created in Twine in under 24 hours for the Voxelles' #momjam!
- One stage (babyhood) with teaser for more,

####Version 0.0.2
######Current Features
- Re-implemented in pure Javascript (sorry Twine!)
- Fun styles and new graphics with animation

######Coming Features
- Audio
- More levels?
- More children?

######Known Bugs
- None